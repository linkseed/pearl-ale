# ale for Pearl

Asynchronous linting/fixing for Vim and Language Server Protocol (LSP) integration

## Details

- Plugin: https://github.com/w0rp/ale
- Pearl: https://github.com/pearl-core/pearl
